﻿using ChoETL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class ChoJsonReader
    {
        public dynamic getAllObjectsFromFile(string fileName)
        {
            List<dynamic> objectsFromFile = new List<dynamic>();
            try
            {
                using (StreamReader Reader = new StreamReader(fileName))
                {
                    foreach (dynamic objectData in new ChoJSONReader(Reader))
                    {
                        objectsFromFile.Add(objectData);
                    }
                }
                return objectsFromFile;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }
    }
}
