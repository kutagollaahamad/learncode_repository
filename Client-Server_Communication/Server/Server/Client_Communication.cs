﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Client_Communication
    {
        public void allowClientConnection(IAsyncResult clientRequest)
        {
            Client_Communication client_Communication = new Client_Communication();
            ServerSocket serverSocket = new ServerSocket();
            Socket client = client_Communication.endEstablishingClientConnection(clientRequest, serverSocket.get());
            client_Communication.addClientToClientList(client);
            client_Communication.startClientServerCommunication(client);
            Console.WriteLine("Client connected, waiting for request...");
            serverSocket.get().BeginAccept(allowClientConnection, null);
        }
        private Socket endEstablishingClientConnection(IAsyncResult clientRequest, Socket server)
        {
            return server.EndAccept(clientRequest);
        }
        private void addClientToClientList(Socket client)
        {
            ClientList clientList = new ClientList();
            clientList.Add(client);
        }

        private void startClientServerCommunication(Socket client)
        {
            Buffer buffer = new Buffer();
            client.BeginReceive(buffer.get(), 0, buffer.getBufferSize(), SocketFlags.None, ReceiveCallback, client);
        }

        private void deleteDisconnectedClientFromClientList(Socket client)
        {
            ClientList clientList = new ClientList();
            clientList.delete(client);
        }

        private string readRequestFromClient(int received, byte[] buffer)
        {
            byte[] requiredBuffer = new byte[received];
            Array.Copy(buffer, requiredBuffer, received);
            string text = Encoding.ASCII.GetString(requiredBuffer);
            Console.WriteLine("Received Text: " + text);
            return text;
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket client = (Socket)AR.AsyncState;
            Client_Communication client_Communication = new Client_Communication();
            int received;
            Buffer buffer = new Buffer();

            try
            {
                received = client.EndReceive(AR);
            }
            catch (SocketException)
            {
                Console.WriteLine("Client forcefully disconnected");
                client.Close();
                client_Communication.deleteDisconnectedClientFromClientList(client);
                return;
            }
            string text = client_Communication.readRequestFromClient(received, buffer.get());
            RequestHandler requestHandler = new RequestHandler();
            requestHandler.serveClientBasedOnRequest(text, client);
            client.BeginReceive(buffer.get(), 0, buffer.getBufferSize(), SocketFlags.None, ReceiveCallback, client);
        }
    }
}
