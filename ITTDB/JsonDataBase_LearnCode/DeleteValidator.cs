﻿using JsonDataBaseLearnCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class DeleteValidator
    {
        public void validate(Type objectType, dynamic propertyValue)
        {
            UserInputTypeValidator validateinput = new UserInputTypeValidator();
            validateinput.rejectNonPrimitiveType(objectType);
            validateinput.rejectPrimitiveData(propertyValue);
        }
    }
}
