﻿using JsonDataBaseLearnCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class Readvalidator
    {
        public void validate(Type objectType)
        {
            UserInputTypeValidator validateinput = new UserInputTypeValidator();
            validateinput.rejectNonPrimitiveType(objectType);
        }
    }
}
