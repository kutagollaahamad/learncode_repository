﻿namespace ITTDataBase
{
    public class Employee
    {
        public int ID { set; get; }
        public string name { set; get; }
        public int roll { set; get; }
        public string company { set; get; }

        public override string ToString()
        {
            return "ID: " + "ID.ToString()" + " name: " + name.ToString() + " roll: " + roll.ToString() + " company: " + company.ToString();
        }
    }
}