﻿using ChoETL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    public class DynamicConversion
    {
        public dynamic setIDProperty(dynamic userData, int startId)
        {
            DataDifferentiator differentiator = new DataDifferentiator();
            if (differentiator.checkingWhetherDataIsSingleObject(userData))
            {
                return addIdPropertyToObject(ToDynamic(userData), startId);
            }
            else
            {
                List<ChoDynamicObject> dynamicUserDataList = new List<ChoDynamicObject>();
                foreach (var Data in userData)
                {
                    dynamicUserDataList.Add(addIdPropertyToObject(ToDynamic(Data), startId++));
                }
                return dynamicUserDataList;
            }
        }
        private dynamic addIdPropertyToObject(dynamic userObject, int value)
        {
            ChoDynamicObject expando = userObject;
            expando.Add("ID", value);
            return expando;
        }
        public dynamic convertUserDataToDynamic(dynamic userData)
        {
            DataDifferentiator differentiator = new DataDifferentiator();
            List<ChoDynamicObject> dynamicUserDataList = new List<ChoDynamicObject>();
            if (differentiator.checkingWhetherDataIsSingleObject(userData))
            {
                return dynamicUserDataList.Add(ToDynamic(userData));
            }
            else
            {
                foreach (var Data in userData)
                {
                    dynamicUserDataList.Add(ToDynamic(Data));
                }
                return dynamicUserDataList;
            }
        }
        public dynamic ToDynamic(object value)
        {
            ChoDynamicObject expando = new ChoDynamicObject();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(value.GetType()))
            {
                expando.Add(property.Name, property.GetValue(value));
            }
            return expando as ChoDynamicObject;
        }
        public dynamic convertUserDataTypeToLocalDynamicType(dynamic userData)
        {
            DataDifferentiator differentiator = new DataDifferentiator();
            if (userData == null)
            {
                throw new Exception("No data to save");
            }
            else if (!differentiator.checkingWhetherDataIsSingleObject(userData))
            {
                return castToDynamic(userData);
            }
            else
            {
                return ToDynamic(userData);
            }
        }
        private dynamic castToDynamic(dynamic userData)
        {
            List<dynamic> dynamicList = new List<dynamic>();
            foreach (var obj in userData)
            {
                dynamicList.Add(ToDynamic(obj));
            }
            return dynamicList;
        }
    }
}
