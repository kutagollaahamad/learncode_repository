﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class FileValidation
    {
        public void fileExistanceCheck(string fileName)
        {
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("File Not Found");
            }
        }
        public void emptyFileCheck(string fileName)
        {
            if (new FileInfo(fileName).Length == 0 || File.ReadAllText(fileName) == "[]")
            {
                throw new InvalidOperationException("Empty File");
            }
        }
    }
}
