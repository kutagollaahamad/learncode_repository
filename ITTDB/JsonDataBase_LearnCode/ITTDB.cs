﻿using System;
using JsonDataBaseLearnCode;

namespace ITTDB
{
    public static class ITTDBCRUD
    {
        public static int save(dynamic userData, bool autoSetID = false)
        {
            Manager manager = new Manager();
            return manager.save(userData, autoSetID);
        }
        public static dynamic read(Type objectType)
        {
            Manager manager = new Manager();
            return  manager.read(objectType);
        }
        public static dynamic find(Type objectType, dynamic propertyValue, string propertyName)
        {
            Manager manager = new Manager();
            return manager.find(objectType, propertyValue, propertyName);
        }
        public static int edit(dynamic userObject)
        {
            Manager manager = new Manager();
            return manager.edit(userObject);
        }
        public static int delete(Type objectType, dynamic propertyValue, string propertyName)
        {
            Manager manager = new Manager();
            return manager.delete(objectType, propertyValue, propertyName);
        }
        public static void detailsofITTDB()
        {
            Manager manager = new Manager();
            manager.detailsofITTDB();
        }
    }
}