﻿using ITTDataBase;
using ITTDB;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBNUnitTest
{
    [TestFixture]
    public class ITTDBEditTest
    {
        [SetUp]
        public void TestInitialize()
        {
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
        }
        [Test]
        public void Test_Edit_Method_Return_Value()
        {
            // Arrange
            Student student = new Student()
            {
                ID = 1,
                Name = "Kinza",
                Year = 1997
            };
            int expected = 1;

            // Act
            var actual = ITTDBCRUD.edit(student);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Test_Edit_Method_Edited_Data()
        {
            // Arrange
            Student student = new Student()
            {
                ID = 1,
                Name = "Kinza",
                Year = 1997
            };

            // Act
            ITTDBCRUD.edit(student);
            var actual = ((List<dynamic>)ITTDBCRUD.read(typeof(Student))).Find(x => x.ID == 1);

            // Assert
            Assert.AreEqual(Helper.areEqualObjects(actual, student), true);
        }
        [Test]
        public void Test_Edit_Method_By_Passing_Primitive_Data_Types()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<TypeAccessException>(() => ITTDBCRUD.edit(1));

            // Assert
            Assert.AreEqual("Primitive data types are not acceptable", exception.Message);
        }
        [Test]
        public void Test_Edit_Method_For_Empty_DataBase()
        {
            // Arrange
            File.WriteAllText("Student.json", "[]");
            Student student = new Student()
            {
                ID = 1,
                Name = "Ahamad",
                Year = 1997
            };

            // Act
            var exception = Assert.Throws<InvalidOperationException>(() => ITTDBCRUD.edit(student));

            // Assert
            Assert.AreEqual("Empty File", exception.Message);
        }
        [Test]
        public void Test_Edit_Method_By_Passing_List()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;
            Student student1 = new Student();
            student1.ID = 1;
            student1.Name = "Ahamad";
            student1.Year = 1997;
            List<Student> studentList = new List<Student>();
            studentList.Add(student);
            studentList.Add(student1);

            // Act
            var exception = Assert.Throws<InvalidDataException>(() => ITTDBCRUD.edit(studentList));

            // Assert
            Assert.AreEqual("Edit accepts only one Object at a time", exception.Message);
        }
        [TearDown]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
        }
    }
}
