﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class ServerSocket
    {
        private static readonly Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public Socket get()
        {
            return serverSocket;
        }
        public void setupServer()
        {
            Client_Communication client_Communication = new Client_Communication(); 
            Console.WriteLine("Setting up server...");
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, 100));
            serverSocket.Listen(0);
            serverSocket.BeginAccept(client_Communication.allowClientConnection, null);
            Console.WriteLine("Server setup complete");
        }

    }
}
