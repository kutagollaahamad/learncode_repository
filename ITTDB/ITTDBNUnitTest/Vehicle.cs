﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBNUnitTest
{
    public class Vehicle
    {
        public string Name { get; set; }
        public int Model { get; set; }
        public int price { get; set; }

    }
}
