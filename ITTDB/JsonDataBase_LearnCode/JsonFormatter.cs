﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class JsonFormatter
    {
        public void JsonFormat(string fileName)
        {
            try
            {
                string fileData = File.ReadAllText(fileName);
                fileData = fileData.Replace("[", "").Replace("]", "").Replace("}{", "},{");
                fileData = "[" + fileData + "]";
                File.WriteAllText(fileName, fileData);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}
