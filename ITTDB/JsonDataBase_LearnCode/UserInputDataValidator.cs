﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class UserInputDataValidator
    {
        public void nullCheck(dynamic userData)
        {
            if (userData == null)
            {
                throw new NullReferenceException("Data is Null");
            }
        }
        public void propertyExistanceCheck(Type objectType, string propertyName)
        {
            if (objectType.GetProperty(propertyName) == null)
            {
                throw new InvalidFilterCriteriaException("No Such Property Exits");
            }
        }
        public void checkForList(dynamic userObject)
        {
            DataDifferentiator differentiator = new DataDifferentiator();
            if (!differentiator.checkingWhetherDataIsSingleObject(userObject))
            {
                throw new InvalidDataException("Edit accepts only one Object at a time");
            }
        }
    }
}
