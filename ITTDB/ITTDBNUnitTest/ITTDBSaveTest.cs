﻿using ITTDataBase;
using ITTDB;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBNUnitTest
{
    [TestFixture]
    public class ITTDBSaveTest
    {
        [Test]
        public void Test_Save_Method_Return_Value_With_Single_Object()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            int result = ITTDBCRUD.save(student);

            // Assert
            Assert.AreEqual(1, result);
        }

        [Test]
        public void Test_Saved_Data()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            ITTDBCRUD.save(student);
            var actual = ITTDBCRUD.find(typeof(Student), 1, "ID");

            // Assert
            Assert.AreEqual(Helper.areEqualObjects(actual, student), true);
        }

        [Test]
        public void Test_Save_Method_By_Passing_Null_Data()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<NullReferenceException>(() => ITTDBCRUD.save(null));

            // Assert
            Assert.AreEqual("Data is Null", exception.Message);
        }

        [Test]
        public void Test_Save_Method_By_Passing_Primitive_Data_Types()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<TypeAccessException>(() => ITTDBCRUD.save(1));

            // Assert
            Assert.AreEqual("Primitive data types are not acceptable", exception.Message);
        }

        [Test]
        public void Test_Save_Method_With_Duplicate_ID()
        {
            // Arrange
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            var exception = Assert.Throws<InvalidDataException>(() => ITTDBCRUD.save(student));

            // Assert
            Assert.AreEqual("Duplicate Ids are present", exception.Message);
        }

        [Test]
        public void Test_Auto_Set_ID_Of_Save_Method()
        {
            // Arrange
            Vehicle vehicle = new Vehicle();
            vehicle.Name = "Swift";
            vehicle.Model = 2019;
            vehicle.price = 200000;


            // Act
            int result = ITTDBCRUD.save(vehicle, true);

            // Assert
            Assert.IsTrue(result > 0);
        }

        [Test]
        public void Test_Save_Method_With_AutoSet_ID_To_True_And_Passing_ID()
        {
            // Arrange
            Employee employee = new Employee();
            employee.ID = 21;
            employee.name = "ITT";
            employee.roll = 1997;
            employee.company = "ITT";

            // Act
            var exception = Assert.Throws<InvalidDataException>(() => ITTDBCRUD.save(employee, true));

            // Assert
            Assert.AreEqual("ID property already exists in data. AutoIncrement wont works", exception.Message);
        }
        [Test]
        public void Test_Save_Method_With_Wrong_Type_Of_Data()
        {
            // Arrange
            Employee employee = new Employee();
            employee.ID = 21;
            employee.name = "ITT";
            employee.roll = 1997;
            employee.company = "ITT";

            // Act
            var exception = Assert.Throws<InvalidDataException>(() => ITTDBCRUD.save(typeof(Employee), true));

            // Assert
            Assert.AreEqual("Provided Type instead of Data", exception.Message);
        }
        [Test]
        public void Test_Save_Method_Return_Value_With_List_Of_Objects()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;
            Student student1 = new Student();
            student1.ID = 2;
            student1.Name = "Ahamad";
            student1.Year = 1997;
            List<Student> studentList = new List<Student>();
            studentList.Add(student);
            studentList.Add(student1);

            // Act
            int result = ITTDBCRUD.save(studentList);

            // Assert
            Assert.AreEqual(2, result);
        }
        [Test]
        public void Test_Save_Method_With_List_Of_Objects_Of_Duplicate_IDs()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;
            Student student1 = new Student();
            student1.ID = 1;
            student1.Name = "Ahamad";
            student1.Year = 1997;
            List<Student> studentList = new List<Student>();
            studentList.Add(student);
            studentList.Add(student1);

            // Act
            var exception = Assert.Throws<InvalidDataException>(() => ITTDBCRUD.save(studentList));

            // Assert
            Assert.AreEqual("Ids should be uniquerows added", exception.Message);
        }
        [TearDown]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
            File.WriteAllText("Vehicle.json", "[]");
        }
    }
}
