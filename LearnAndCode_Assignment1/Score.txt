
LIVE EVENTS 11
Monk and Chamber of Secrets / Submission (19363225) by Kutagolla Ahamad (kutagolla)
Submission ID: 19363225 / 2 days ago at 11:13 PM
RESULT:  Accepted
Score

20.0

 
Time (sec)

1.73109

 
Memory (KiB)

64

 
Language

C#

Input	Result	Time (sec)	Memory (KiB)	Score	Your Output	Correct Output	Diff
Input #1		0.107986	64	4			
Input #2		0.108703	64	4			
Input #3		0.109117	64	4			
Input #4		0.106807	64	4			
Input #5		0.109123	64	7			
Input #6		0.108261	64	7			
Input #7		0.107271	64	7			
Input #8		0.107624	64	7			
Input #9		0.107976	64	6			
Input #10		0.10677	        64	4			
Input #11		0.108372	64	6			
Input #12		0.108508	64	8			
Input #13		0.10804	        64	8			
Input #14		0.108886	64	8			
Input #15		0.108055	64	8			
Input #16		0.109592	64	8			


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnAndCode_Assignment1
{
    internal class Spider
    {
        public int powerOfSpider;
        public int indexOfSpider;
    }

    internal class PositionsOfMaxPowerSpiders
    {
        public void positions(Queue<Spider> spidersQueue, int numberOfSpiders, int numberOfSpidersToDequeue)
        {
            Spider[] spiderDequeueArray = new Spider[numberOfSpidersToDequeue];

            for (int dequeueIterator = 0; dequeueIterator < numberOfSpidersToDequeue; dequeueIterator++)
            {
                int arrayIterator = 0, indexOfMaxPowerSpider = 0;
                while (spidersQueue.Count > 0 && arrayIterator < numberOfSpidersToDequeue)
                {
                    Spider tempSpider = spidersQueue.Dequeue();
                    spiderDequeueArray[arrayIterator] = tempSpider;
                    if (tempSpider.powerOfSpider > spiderDequeueArray[indexOfMaxPowerSpider].powerOfSpider)
                    {
                        indexOfMaxPowerSpider = arrayIterator;
                    }
                    arrayIterator++;
                }
                Console.Write(spiderDequeueArray[indexOfMaxPowerSpider].indexOfSpider + " ");
                arrayIterator--;
                for (int enqueueIterator = 0; enqueueIterator <= arrayIterator; enqueueIterator++)
                {
                    if (spiderDequeueArray[enqueueIterator].powerOfSpider > 0)
                    {
                        spiderDequeueArray[enqueueIterator].powerOfSpider -= 1;
                    }
                    if (enqueueIterator != indexOfMaxPowerSpider)
                    {
                        spidersQueue.Enqueue(spiderDequeueArray[enqueueIterator]);
                    }
                }
            }

        }
    }

    class MonkAndChamberOfSecrets
    {
        public static void Main(String[] args)
        {
            MonkAndChamberOfSecrets monkAndChamberOfSecrets = new MonkAndChamberOfSecrets();
            monkAndChamberOfSecrets.readInputs();
            
        }
        public void readInputs()
        {
            string[] inputString = Console.ReadLine().Split();
            int numberOfSpiders = int.Parse(inputString[0]);
            int numberOfSpidersToDequeue = int.Parse(inputString[1]);
            Queue<Spider> spidersQueue = new Queue<Spider>();
            string[] powers = Console.ReadLine().Split();
            for (int iterator = 0; iterator < numberOfSpiders; iterator++)
            {
                Spider spiderObject = new Spider
                {
                    powerOfSpider = int.Parse(powers[iterator]),
                    indexOfSpider = iterator + 1
                };
                spidersQueue.Enqueue(spiderObject);
            }
            PositionsOfMaxPowerSpiders positionsOfMaxPowerSpider = new PositionsOfMaxPowerSpiders();
            positionsOfMaxPowerSpider.positions(spidersQueue, numberOfSpiders, numberOfSpidersToDequeue);

        }
    }
}




