﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class ClientList
    {
        private static readonly List<Socket> clientSockets = new List<Socket>();
        public void Add(Socket socket)
        {
            clientSockets.Add(socket);
        }
        public void delete(Socket socket)
        {
            clientSockets.Remove(socket);
        }
        public List<Socket> read()
        {
            return clientSockets;
        }
    }
}
