﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ITTDataBase;
using ITTDB;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace ITTDBUnitTest
{
    [TestClass]
    public class ITTDBSaveTest
    {
        [TestMethod]
        public void Test_Save_Method_Return_Value_With_Single_Object()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            int result = ITTDBCRUD.save(student);

            // Assert
           Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void Test_Saved_Data()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            ITTDBCRUD.save(student);
            var actual = ITTDBCRUD.find(typeof(Student), 1, "ID");

            // Assert
            Assert.AreEqual(Helper.areEqualObjects(actual, student), true);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void Test_Save_Method_By_Passing_Null_Data()
        {
            // Arrange

            // Act
            int result = ITTDBCRUD.save(null);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(TypeAccessException))]
        public void Test_Save_Method_By_Passing_Primitive_Data_Types()
        {
            // Arrange

            // Act
            int result = ITTDBCRUD.save(1);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void Test_Save_Method_With_Duplicate_ID()
        {
            // Arrange
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            ITTDBCRUD.save(student);

            // Assert
        }

        [TestMethod]
        public void Test_Auto_Set_ID_Of_Save_Method()
        {
            // Arrange
            Vehicle vehicle = new Vehicle();
            vehicle.Name = "Swift";
            vehicle.Model = 2019;
            vehicle.price = 200000;


            // Act
            int result = ITTDBCRUD.save(vehicle, true);

            // Assert
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void Test_Save_Method_With_AutoSet_ID_To_True_And_Passing_ID()
        {
            // Arrange
            Employee employee = new Employee();
            employee.ID = 21;
            employee.name = "ITT";
            employee.roll = 1997;
            employee.company = "ITT";

            // Act
            int result = ITTDBCRUD.save(employee, true);

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void Test_Save_Method_With_Wrong_Type_Of_Data()
        {
            // Arrange
            Employee employee = new Employee();
            employee.ID = 21;
            employee.name = "ITT";
            employee.roll = 1997;
            employee.company = "ITT";

            // Act
            int result = ITTDBCRUD.save(typeof(Employee), true);

            // Assert
        }
        [TestMethod]
        public void Test_Save_Method_Return_Value_With_List_Of_Objects()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;
            Student student1 = new Student();
            student1.ID = 2;
            student1.Name = "Ahamad";
            student1.Year = 1997;
            List<Student> studentList = new List<Student>();
            studentList.Add(student);
            studentList.Add(student1);

            // Act
            int result = ITTDBCRUD.save(studentList);

            // Assert
            Assert.AreEqual(2, result);
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidDataException))]
        public void Test_Save_Method_With_List_Of_Objects_Of_Duplicate_IDs()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;
            Student student1 = new Student();
            student1.ID = 1;
            student1.Name = "Ahamad";
            student1.Year = 1997;
            List<Student> studentList = new List<Student>();
            studentList.Add(student);
            studentList.Add(student1);

            // Act
            ITTDBCRUD.save(studentList);

            // Assert
        }
        [TestCleanup]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
            File.WriteAllText("Vehicle.json", "[]");
        }
    }
}
