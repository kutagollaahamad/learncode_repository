﻿using JsonDataBaseLearnCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class ModifyValidator
    {
        public void validate(dynamic userObject)
        {
            UserInputTypeValidator validateinput = new UserInputTypeValidator();
            UserInputDataValidator validateUserdata = new UserInputDataValidator();
            validateUserdata.nullCheck(userObject);
            validateUserdata.checkForList(userObject);
            validateinput.rejectNonPrimitiveData(userObject);
            validateUserdata.propertyExistanceCheck(userObject.GetType(), "ID");
        }
    }
}
