﻿using ChoETL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    public class SetId
    {
        public dynamic checkAutoSetID(dynamic userData,bool autoSetID, string fileName)
        {
            if(autoSetID)
            {
                checkId(userData);
                return autoSetId(userData, fileName);
            }
            else
            {
                DBValidation dbValidation = new DBValidation();
                dbValidation.dataValidations(userData, fileName);
                return userData;
            }
        }
        public void checkId(dynamic userData)
        {
            DataDifferentiator differentiator = new DataDifferentiator();
            if (differentiator.checkingWhetherDataIsSingleObject(userData))
            {
                searchProperty(userData);
            }
            else
            {
                foreach (var obj in userData)
                {
                    searchProperty(obj);
                }
            }
        }
        private void searchProperty(dynamic data)
        {
            foreach (var property in GetProperties(data))
            {
                if (property.Name.ToLower() == "id")
                {
                    throw new InvalidDataException("ID property already exists in data. AutoIncrement wont works");
                }
            }
        }
        private PropertyInfo[] GetProperties(object obj)
        {
            return obj.GetType().GetProperties();
        }
        public dynamic autoSetId(dynamic userData, string fileName)
        {
            DynamicConversion dynamicConversion = new DynamicConversion();
            if (!File.Exists(fileName) || new FileInfo(fileName).Length == 0 || File.ReadAllText(fileName) == "[]")
            {
                return dynamicConversion.setIDProperty(userData, 1);
            }
            else
            {
                FileOperations file = new FileOperations();
                return dynamicConversion.setIDProperty(userData, findMaximumId(file.readFile(fileName)) + 1);
            }
        }
        public int findMaximumId(dynamic dynamicUserData)
        {
            dynamic maxId = dynamicUserData[0]["Id"];
            foreach (var data in dynamicUserData)
            {
                if (maxId < data["id"])
                {
                    maxId = data["id"];
                }
            }
            return maxId;
        }
    }
}
