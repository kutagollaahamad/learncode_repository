﻿using ChoETL;
using ITTDB;
using JsonDataBaseLearnCode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    public class DBValidation
    {
        public void dataValidations(dynamic userData, string fileName)
        {
            DynamicConversion dynamicConversion = new DynamicConversion();
            dynamic dynamicTypeData = dynamicConversion.convertUserDataTypeToLocalDynamicType(userData);
            DataDifferentiator differentiator = new DataDifferentiator();
            FileOperations file = new FileOperations();
            if (!differentiator.checkingWhetherDataIsSingleObject(userData))
            {
                checkingForDuplicateIdsWithinUserData(dynamicTypeData);

                List<dynamic> previousDataInTheFile = file.getPreviousData(fileName);
                dataValidationForMultipleData(dynamicTypeData, previousDataInTheFile);
            }
            else
            {
                List<dynamic> previousDataInTheFile = file.getPreviousData(fileName);
                dataValidationForSingleData(dynamicTypeData, previousDataInTheFile);
            }
        }
        public void checkingForDuplicateIdsWithinUserData(dynamic dynamicListOfObjects)
        {
            for (int i = 0; i < dynamicListOfObjects.Count; i++)
            {
                for (int j = i + 1; j < dynamicListOfObjects.Count; j++)
                {
                    if (dynamicListOfObjects[i]["id"] == dynamicListOfObjects[j]["id"])
                    {
                        throw new InvalidDataException("Ids should be unique" + "rows added");
                    }
                }
            }
        }
        public void dataValidationForMultipleData(dynamic dynamicTypeData, dynamic previousDataInTheFile)
        {
            if (previousDataInTheFile != null)
            {
                forMultipleDataCheckForDuplicateIdsWithinCurrentAndPreviousData(dynamicTypeData, previousDataInTheFile);
            }
        }
        private void forMultipleDataCheckForDuplicateIdsWithinCurrentAndPreviousData(dynamic userData, List<dynamic> previousData)
        {
            foreach (var obj in userData)
            {
                if (previousData.FindIndex(x => x["id"] == obj["id"]) >= 0)
                {
                    throw new Exception("Duplicate Ids are present");
                }
            }
        }
        public void dataValidationForSingleData(dynamic dynamicTypeData, dynamic previousDataInTheFile)
        {
            if (previousDataInTheFile != null)
            {
                forSingleDataCheckForDuplicateIdsWithinCurrentAndPreviousData(dynamicTypeData, previousDataInTheFile);
            }
        }
        private void forSingleDataCheckForDuplicateIdsWithinCurrentAndPreviousData(dynamic userData, List<dynamic> previousData)
        {
            if (previousData.FindIndex(x => x["id"] == userData["id"]) >= 0)
            {
                throw new InvalidDataException("Duplicate Ids are present");
            }
        }
    }
}
