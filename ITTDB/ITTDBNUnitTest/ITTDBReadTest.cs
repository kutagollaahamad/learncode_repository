﻿using ITTDataBase;
using ITTDB;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBNUnitTest
{
    [TestFixture]
    public class ITTDBReadTest
    {
        [SetUp]
        public void TestInitialize()
        {
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
        }
        [Test]
        public void Test_Read_Method_Return_Value()
        {
            // Arrange
            int expected = 1;

            // Act
            var actual = ITTDBCRUD.read(typeof(Student)).Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Test_Read_Method_And_Validate_Data()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            var actual = ((List<dynamic>)ITTDBCRUD.read(typeof(Student))).Find(x => x.ID == 1);

            // Assert
            Assert.AreEqual(Helper.areEqualObjects(actual, student), true);
        }
        [Test]
        public void Test_Read_Method_For_Empty_DataBase()
        {
            // Arrange
            File.WriteAllText("Student.json", "[]");

            // Act
            var exception = Assert.Throws<InvalidOperationException>(() => ITTDBCRUD.read(typeof(Student)));

            // Assert
            Assert.AreEqual("Empty File", exception.Message);
        }
        [Test]
        public void Test_Read_Method_By_Passing_Primitive_Data_Types()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<TypeAccessException>(() => ITTDBCRUD.read(typeof(int)));

            // Assert
            Assert.AreEqual("Primitive data types are not acceptable", exception.Message);
        }
        [Test]
        public void Test_Read_Method_By_Passing_Unknown_Data_Type_For_DB()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<FileNotFoundException>(() => ITTDBCRUD.read(typeof(Object)));

            // Assert
            Assert.AreEqual("File Not Found", exception.Message);
        }
        [TearDown]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
        }
    }
}
