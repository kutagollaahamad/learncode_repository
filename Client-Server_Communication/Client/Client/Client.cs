﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Client
    {
        static void Main()
        {
            Console.Title = "Client";
            Connection connection = new Connection();
            Communication communication = new Communication();
            connection.ConnectToServer();
            communication.RequestLoop();
            communication.Exit();
        }
    }
}
