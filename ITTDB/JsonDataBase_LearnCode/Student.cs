﻿namespace ITTDataBase
{
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }

        public override string ToString()
        {
            return "ID: " + ID.ToString() + " Name: " + Name.ToString() + " Year: " + Year.ToString();
        }
    }
}