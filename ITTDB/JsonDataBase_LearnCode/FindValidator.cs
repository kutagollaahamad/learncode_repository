﻿using JsonDataBaseLearnCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class FindValidator
    {
        public void validate(Type objectType, dynamic propertyValue, string propertyName)
        {
            UserInputTypeValidator validateinput = new UserInputTypeValidator();
            UserInputDataValidator validateUserdata = new UserInputDataValidator();
            validateinput.rejectNonPrimitiveType(objectType);
            validateinput.rejectPrimitiveData(propertyValue);
            validateUserdata.propertyExistanceCheck(objectType, propertyName);
        }
    }
}
