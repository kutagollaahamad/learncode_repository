﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class UserInputTypeValidator
    {
        public void rejectPrimitiveData(dynamic userData)
        {
            if (!primitiveCheck(userData))
            {
                throw new TypeAccessException("Non Primitive data types are not acceptable");
            }
        }
        public void rejectNonPrimitiveData(dynamic userData)
        {
            if (primitiveCheck(userData))
            {
                throw new TypeAccessException("Primitive data types are not acceptable");
            }
        }
        public void rejectPrimitiveType(Type t)
        {
            if (!primitiveTypeCheck(t))
            {
                throw new TypeAccessException("Non Primitive data types are not acceptable");
            }
        }
        public void rejectNonPrimitiveType(Type t)
        {
            if (primitiveTypeCheck(t))
            {
                throw new TypeAccessException("Primitive data types are not acceptable");
            }
        }
        public bool primitiveCheck(dynamic userData)
        {
            DataDifferentiator differentiator = new DataDifferentiator();
            if (differentiator.checkingWhetherDataIsSingleObject(userData) && !(userData is Type))
            {
                return primitiveTypeCheck(userData.GetType());
            }
            else if (!(userData is Type))
            {
                return primitiveTypeCheck(userData[0].GetType());
            }
            else
            {
                throw new InvalidDataException("Provided Type instead of Data");
            }
        }
        public bool primitiveTypeCheck(Type t)
        {
            if (t.IsPrimitive || t == typeof(Decimal) || t == typeof(String))
            {
                return true;
            }
            return false;
        }
    }
}
