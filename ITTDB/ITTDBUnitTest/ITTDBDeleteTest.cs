﻿using ITTDataBase;
using ITTDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;


namespace ITTDBUnitTest
{
    [TestClass]
    public class ITTDBDeleteTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
        }
        [TestMethod]
        public void Test_Delete_Method_Return_Value()
        {
            // Arrange
            int expected = 1;

            // Act
            //ITTDBCRUD.save(student);
            var actual = ITTDBCRUD.delete(typeof(Student), 1, "ID");

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_Delete_Method_For_Deleted_Data()
        {
            // Arrange

            // Act
            ITTDBCRUD.delete(typeof(Student), 1, "ID");
            var actual = ((List<dynamic>)ITTDBCRUD.read(typeof(Student))).Find(x => x.ID == 1);

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidFilterCriteriaException))]
        public void Test_Delete_Method_With_Non_Existing_Property()
        {
            // Arrange

            // Act
            int actual = ITTDBCRUD.delete(typeof(Student), 2, "Roll");

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(TypeAccessException))]
        public void Test_Delete_Method_By_Passing_Primitive_Data_Types_InsteadOf_Object_Type()
        {
            // Arrange

            // Act
            int actual = ITTDBCRUD.delete(typeof(int), 1, "ID");

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(TypeAccessException))]
        public void Test_Delete_Method_By_Passing_NonPrimitive_Data_Types_InsteadOf_Primitive_Type()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            int actual = ITTDBCRUD.delete(typeof(int), student, "ID");

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_Delete_Method_For_Empty_DataBase()
        {
            // Arrange
            ITTDBCRUD.delete(typeof(Student), 1, "ID");

            // Act
            int result = ITTDBCRUD.delete(typeof(Student), 1, "ID");

            // Assert
        }
        [TestCleanup]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
        }
    }
}
