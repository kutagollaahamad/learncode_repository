﻿using ITTDataBase;
using ITTDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBUnitTest
{
    [TestClass]
    public class ITTDBReadTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
        }
        [TestMethod]
        public void Test_Read_Method_Return_Value()
        {
            // Arrange
            int expected = 1;

            // Act
            var actual = ITTDBCRUD.read(typeof(Student)).Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Test_Read_Method_And_Validate_Data()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            var actual = ((List<dynamic>)ITTDBCRUD.read(typeof(Student))).Find(x => x.ID == 1);

            // Assert
            Assert.AreEqual(Helper.areEqualObjects(actual, student), true);
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_Read_Method_For_Empty_DataBase()
        {
            // Arrange
            File.WriteAllText("Student.json", "[]");

            // Act
            int result = ITTDBCRUD.read(typeof(Student));

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(TypeAccessException))]
        public void Test_Read_Method_By_Passing_Primitive_Data_Types()
        {
            // Arrange

            // Act
            int result = ITTDBCRUD.read(typeof(int));

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void Test_Read_Method_By_Passing_Unknown_Data_Type_For_DB()
        {
            // Arrange

            // Act
            int result = ITTDBCRUD.read(typeof(Object));

            // Assert
        }
        [TestCleanup]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
        }
    }
}
