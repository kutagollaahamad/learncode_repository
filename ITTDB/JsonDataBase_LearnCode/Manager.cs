﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace JsonDataBaseLearnCode
{
    public class Manager
    {
        public int save(dynamic userData, bool autoSetID)
        {
            SaveValidator validator = new SaveValidator();
            validator.Validate(userData);
            FileOperations file = new FileOperations();
            string fileName = file.getFileName(userData);
            SetId setId = new SetId();
            userData = setId.checkAutoSetID(userData, autoSetID, fileName);
            file.writeDataintoFile(fileName, userData);
            return file.numberOfChangesMadeToFile(fileName);
        }
        public dynamic read(Type objectType)
        {
            Readvalidator validator = new Readvalidator();
            validator.validate(objectType);
            string fileName = objectType.Name + ".json";
            FileOperations file = new FileOperations();
            return file.readFile(fileName);
        }
        public dynamic find(Type objectType, dynamic propertyValue, string propertyName)
        {
            FindValidator validator = new FindValidator();
            validator.validate(objectType, propertyValue, propertyName);
            List<dynamic> allObjectsFromFile = read(objectType);
            object foundObject = allObjectsFromFile.Find(x => x[propertyName] == propertyValue);
            return foundObject;
        }
        public int edit(dynamic userObject)
        {
            ModifyValidator validator = new ModifyValidator();
            validator.validate(userObject);
            DynamicConversion dynamicConversion = new DynamicConversion();
            dynamic dynamicObject = dynamicConversion.ToDynamic(userObject);
            FileOperations file = new FileOperations();
            string fileName = file.getFileName(userObject);
            List<dynamic> list = file.readFile(fileName);
            int index = list.FindIndex(x => x["id"] == dynamicObject["id"]);
            UpdateData updateData = new UpdateData();
            return updateData.replaceUpdatedObject(index, list, fileName, userObject);
        }
        public int delete(Type objectType, dynamic propertyValue, string propertyName)
        {
            DeleteValidator validator = new DeleteValidator();
            UserInputDataValidator validateUserdata = new UserInputDataValidator();
            validator.validate(objectType, propertyValue);
            string fileName = objectType.Name + ".json";
            validateUserdata.propertyExistanceCheck(objectType, propertyName);
            List<dynamic> allObjectsFromFile = read(objectType);
            int index = allObjectsFromFile.FindIndex(x => x[propertyName] == propertyValue);
            UpdateData updateData = new UpdateData();
            return updateData.deleteObject(index, allObjectsFromFile, fileName);
        }
        public void detailsofITTDB()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string path = System.IO.Path.GetDirectoryName(assembly.Location);
            DirectoryInfo directory = new DirectoryInfo(path);
            FileInfo[] files = directory.GetFiles("*.json");
            int numberFiles = files.Length;
            Console.WriteLine("Directory where Files are stored : " + files[0].DirectoryName);
            Console.WriteLine("Number of Files in Data Base: " + numberFiles);
            for (int i = 0; i < numberFiles; i++)
            {
                Console.WriteLine();
                Console.WriteLine("File : {0}", i + 1);
                Console.WriteLine("Number of Objects in the File : " + File.ReadAllText(files[i].Name).Count(x => x == '{'));
                DateTime dtCreationTime = files[i].CreationTime;
                Console.WriteLine("File's Name:" + files[i].Name + Environment.NewLine +
                    "File total Size: " + files[i].Length.ToString() + Environment.NewLine +
                    "Date and Time File Created: " + dtCreationTime.ToString() + Environment.NewLine +
                    "File Extension: " + files[i].Extension);
            }
        }
    }
}
