﻿using ITTDataBase;
using ITTDB;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBNUnitTest
{
    [TestFixture]
    public class ITTDBFindTest
    {
        [SetUp]
        public void TestInitialize()
        {
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
        }
        [Test]
        public void Test_Find_Method_Return_Value()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            var std = ITTDBCRUD.find(typeof(Student), 1, "ID");

            // Assert
            Assert.AreEqual(Helper.areEqualObjects(std, student), true);
        }
        [Test]
        public void Test_Find_Method_With_Non_Existing_Property()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<InvalidFilterCriteriaException>(() => ITTDBCRUD.find(typeof(Student), 2, "Roll"));

            // Assert
            Assert.AreEqual("No Such Property Exits", exception.Message);
        }
        [Test]
        public void Test_Find_Method_By_Passing_Primitive_Data_Types_InsteadOf_Object_Type()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<TypeAccessException>(() => ITTDBCRUD.find(typeof(int), 1, "ID"));

            // Assert
            Assert.AreEqual("Primitive data types are not acceptable", exception.Message);
        }
        [Test]
        public void Test_Find_Method_By_Passing_NonPrimitive_Data_Types_InsteadOf_Primitive_Type()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            var exception = Assert.Throws<TypeAccessException>(() => ITTDBCRUD.find(typeof(int), student, "ID"));

            // Assert
            Assert.AreEqual("Primitive data types are not acceptable", exception.Message);
        }
        [Test]
        public void Test_Find_Method_For_Empty_DataBase()
        {
            // Arrange
            File.WriteAllText("Student.json", "[]");

            // Act
            var exception = Assert.Throws<InvalidOperationException>(() => ITTDBCRUD.find(typeof(Student), 1, "ID"));

            // Assert
            Assert.AreEqual("Empty File", exception.Message);
        }
        [TearDown]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
        }
    }
}
