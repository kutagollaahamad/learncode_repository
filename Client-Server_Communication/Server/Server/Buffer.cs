﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Buffer
    {
        private const int BUFFER_SIZE = 2048;
        private static readonly byte[] buffer = new byte[BUFFER_SIZE];
        public byte[] get()
        {
            return buffer;
        }
        public int getBufferSize()
        {
            return BUFFER_SIZE;
        }
    }
}
