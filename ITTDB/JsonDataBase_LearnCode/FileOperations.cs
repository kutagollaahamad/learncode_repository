﻿using ChoETL;
using System;
using System.Collections.Generic;
using System.IO;
using ITTDB;
using System.ComponentModel;
using JsonDataBaseLearnCode;
using Newtonsoft.Json;
using System.Linq;

namespace JsonDataBaseLearnCode
{
    public class FileOperations
    {
        public void writeDataintoFile(string fileName, dynamic userData, bool overrite = true)
        {
            try
            {
                using (TextWriter writer = new StreamWriter(fileName, overrite))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, userData);
                }
                fileModificationToJsonFormat(fileName);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }
        public dynamic readFile(string fileName)
        {
            FileValidation fileValidator = new FileValidation();
            fileValidator.fileExistanceCheck(fileName);
            fileValidator.emptyFileCheck(fileName);
            ChoJsonReader reader = new ChoJsonReader();
            List<dynamic> allObjectsFromFile = reader.getAllObjectsFromFile(fileName);
            return allObjectsFromFile;
        }
        public string getFileName(dynamic userData)
        {
            DataDifferentiator differentiator = new DataDifferentiator();
            return differentiator.checkingWhetherDataIsSingleObject(userData) ? userData.GetType().Name + ".json" : userData[0].GetType().Name + ".json";
        }
        public int numberOfChangesMadeToFile(string fileName)
        {
            return File.ReadAllText(fileName).Count(x => x == '{');
        }
        public void fileModificationToJsonFormat(string fileName)
        {
            JsonFormatter formatter = new JsonFormatter();
            formatter.JsonFormat(fileName);
        }
        public List<dynamic> getPreviousData(string fileName)
        {
            if (File.Exists(fileName) && new FileInfo(fileName).Length != 0 && File.ReadAllText(fileName) != "[]")
            {
                return readFile(fileName);
            }
            return null;
        }

    }
}

