﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class UpdateData
    {
        public int replaceUpdatedObject(int index, List<dynamic> list, string fileName, dynamic userObject)
        {
            if (index >= 0)
            {
                list[index] = userObject;
                FileOperations file = new FileOperations();
                file.writeDataintoFile(fileName, list, false);
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public int deleteObject(int index, List<dynamic> allObjectsFromFile, string fileName)
        {
            if (index >= 0)
            {
                allObjectsFromFile.RemoveAt(index);
                FileOperations file = new FileOperations();
                file.writeDataintoFile(fileName, allObjectsFromFile, false);
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}
