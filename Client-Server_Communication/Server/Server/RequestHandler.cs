﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class RequestHandler
    {
        public void serveClientBasedOnRequest(string text, Socket client)
        {
            RequestHandler requestHandler = new RequestHandler();
            if (text.ToLower() == "get time")
            {
                requestHandler.serverClientRequest(client);
            }
            else if (text.ToLower() == "exit")
            {
                requestHandler.closeConnection(client);
                return;
            }
            else
            {
                requestHandler.warnClient(client);
            }
        }
        private void serverClientRequest(Socket client)
        {
            Console.WriteLine("Text is a get time request");
            byte[] data = Encoding.ASCII.GetBytes(DateTime.Now.ToLongTimeString());
            client.Send(data);
            Console.WriteLine("Time sent to client");
        }
        private void closeConnection(Socket client)
        {
            client.Shutdown(SocketShutdown.Both);
            client.Close();
            ClientList clientList = new ClientList();
            clientList.delete(client);
            Console.WriteLine("Client disconnected");
        }
        private void warnClient(Socket client)
        {
            Console.WriteLine("Text is an invalid request");
            byte[] data = Encoding.ASCII.GetBytes("Invalid request");
            client.Send(data);
            Console.WriteLine("Warning Sent");
        }
    }
}
