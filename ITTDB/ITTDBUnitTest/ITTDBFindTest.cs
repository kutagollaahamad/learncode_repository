﻿using ITTDataBase;
using ITTDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBUnitTest
{
    [TestClass]
    public class ITTDBFindTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
        }
        [TestMethod]
        public void Test_Find_Method_Return_Value()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            var std = ITTDBCRUD.find(typeof(Student), 1, "ID");

            // Assert
            Assert.AreEqual(Helper.areEqualObjects(std, student), true);
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidFilterCriteriaException))]
        public void Test_Find_Method_With_Non_Existing_Property()
        {
            // Arrange

            // Act
            int result = ITTDBCRUD.find(typeof(Student), 2, "Roll");

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(TypeAccessException))]
        public void Test_Find_Method_By_Passing_Primitive_Data_Types_InsteadOf_Object_Type()
        {
            // Arrange

            // Act
            int result = ITTDBCRUD.find(typeof(int), 1, "ID");

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(TypeAccessException))]
        public void Test_Find_Method_By_Passing_NonPrimitive_Data_Types_InsteadOf_Primitive_Type()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            int result = ITTDBCRUD.find(typeof(int), student, "ID");

            // Assert
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_Find_Method_For_Empty_DataBase()
        {
            // Arrange
            File.WriteAllText("Student.json", "[]");

            // Act
            int result = ITTDBCRUD.find(typeof(Student), 1, "ID");

            // Assert
        }
        [TestCleanup]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
        }
    }
}
