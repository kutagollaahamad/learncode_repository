﻿using ITTDataBase;
using ITTDB;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;


namespace ITTDBNUnitTest
{
    [TestFixture]
    public class ITTDBDeleteTest
    {
        [SetUp]
        public void TestInitialize()
        {
            File.WriteAllText("Student.json", "[{\"ID\":1,\"Name\":\"Ahamad\",\"Year\":1997}]");
        }
        [Test]
        public void Test_Delete_Method_Return_Value()
        {
            // Arrange
            int expected = 1;

            // Act
            //ITTDBCRUD.save(student);
            var actual = ITTDBCRUD.delete(typeof(Student), 1, "ID");

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Test_Delete_Method_For_Deleted_Data()
        {
            // Arrange

            // Act
            ITTDBCRUD.delete(typeof(Student), 1, "ID");
            var exception = Assert.Throws<InvalidOperationException>(() => ((List<dynamic>)ITTDBCRUD.read(typeof(Student))).Find(x => x.ID == 1));

            // Assert
            Assert.AreEqual("Empty File", exception.Message);
        }
        [Test]
        public void Test_Delete_Method_With_Non_Existing_Property()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<InvalidFilterCriteriaException>(() => ITTDBCRUD.delete(typeof(Student), 2, "Roll"));

            // Assert
            Assert.AreEqual("No Such Property Exits", exception.Message);
        }
        [Test]
        public void Test_Delete_Method_By_Passing_Primitive_Data_Types_InsteadOf_Object_Type()
        {
            // Arrange

            // Act
            var exception = Assert.Throws<TypeAccessException>(() => ITTDBCRUD.delete(typeof(int), 1, "ID"));

            // Assert
            Assert.AreEqual("Primitive data types are not acceptable", exception.Message);
        }
        [Test]
        public void Test_Delete_Method_By_Passing_NonPrimitive_Data_Types_InsteadOf_Primitive_Type()
        {
            // Arrange
            Student student = new Student();
            student.ID = 1;
            student.Name = "Ahamad";
            student.Year = 1997;

            // Act
            var exception = Assert.Throws<TypeAccessException>(() => ITTDBCRUD.delete(typeof(int), student, "ID"));

            // Assert
            Assert.AreEqual("Primitive data types are not acceptable", exception.Message);
        }
        [Test]
        public void Test_Delete_Method_For_Empty_DataBase()
        {
            // Arrange
            ITTDBCRUD.delete(typeof(Student), 1, "ID");

            // Act
            var exception = Assert.Throws<InvalidOperationException>(() => ITTDBCRUD.delete(typeof(Student), 1, "ID"));

            // Assert
            Assert.AreEqual("Empty File", exception.Message);
        }
        [TearDown]
        public void TestCleanup()
        {
            File.WriteAllText("Student.json", "[]");
        }
    }
}
