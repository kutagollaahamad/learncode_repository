﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Connection
    {
        public void ConnectToServer()
        {
            ClientSocket clientSocket = new ClientSocket();
            tryToConnect(clientSocket.get());
            Console.WriteLine("Connected");
        }
        private void tryToConnect(Socket clientSocket)
        {
            int attempts = 0;
            while (!clientSocket.Connected)
            {
                try
                {
                    attempts++;
                    Console.WriteLine("Connection attempt " + attempts);
                    clientSocket.Connect(IPAddress.Loopback, 100);
                }
                catch (SocketException)
                {
                    Console.Clear();
                }
            }
        }
    }
}
