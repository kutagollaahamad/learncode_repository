﻿using JsonDataBaseLearnCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonDataBaseLearnCode
{
    class SaveValidator
    {
        public void Validate(dynamic userData)
        {
            UserInputTypeValidator validateinput = new UserInputTypeValidator();
            UserInputDataValidator validateUserdata = new UserInputDataValidator();
            validateUserdata.nullCheck(userData);
            validateinput.rejectNonPrimitiveData(userData);
        }
    }
}
